#!/usr/bin/env bash

# Git repo at https://github.com/nytimes/covid-19-data
cd covid-19-data
git pull
cd ..

# Git repo at https://github.com/CSSEGISandData/COVID-19
cd COVID-19-jh
git pull
cd ..

# Run twitter
